jQuery(document).ready(function($) {

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
  });


  //remove mobile menu after link click
  if ($(window).width() < 768) {
    $('.nav__item').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }


  //smooth operator
  $('a[href^="#"]').on('click',function (e) {
    e.preventDefault();

    var target = this.hash;
    var $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });


  //in viewport check
  var $animation_elements = $('.anim');
  var $window = $(window);

  function check_if_in_view() {
    var window_height = $window.height();
    var window_top_position = $window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);
   
    $.each($animation_elements, function() {
      var $element = $(this);
      var element_height = $element.outerHeight();
      var element_top_position = $element.offset().top;
      var element_bottom_position = (element_top_position + element_height);
   
      //check to see if this current container is within viewport
      if ((element_bottom_position >= window_top_position) &&
        (element_top_position <= window_bottom_position)) {
        $element.addClass('in-view');
      } else {
        $element.removeClass('in-view');
      }
    });
  }

  $window.on('scroll resize', check_if_in_view);
  $window.trigger('scroll');


  // Initiate the footer
  siteFooter();
  // could be simplified but I want to make it as easy to edit as possible
  $(window).resize(function() {
    siteFooter();
  });
  
  function siteFooter() {
    var siteContent = $('main');
    var siteContentHeight = siteContent.height();
    var siteContentWidth = siteContent.width();

    var siteFooter = $('.footer');
    var siteFooterHeight = siteFooter.height();
    var siteFooterWidth = siteFooter.width();

    siteContent.css({
      "margin-bottom" : siteFooterHeight 
    });
  };


  //owl slider/carousel
  var owl = $(".slider__track");

  owl.owlCarousel({
      loop: true,
      items: 1,
      autoplay: false,
      nav: true,
      mouseDrag: false,
      touchDrag: false,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',  
      navText : ["<i class='fas fa-arrow-left' aria-hidden='true'></i>", "<i class='fas fa-arrow-right' aria-hidden='true'></i>"]
  });

});
