<?php 
	$title = get_field('column_title', 'options');
	$text = get_field('column_text', 'options');
	$img = get_field('img', 'options'); 
?>

<footer class="footer bg--gray-dark" itemscope itemtype="http://schema.org/WPFooter">
	<div class="footer__img" style="background-image: url(<?php echo $img['url']; ?>);"></div>
	<div class="wrap hpad clearfix">
		<div class="row">

			 <div class="col-sm-12 footer__item center">
			 	<div class="footer__scroll-top">
					<a class="btn btn--round footer__btn" href="#header"></a> 
				</div>
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
