<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'hero' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'hero' ); ?>

    <?php
    } elseif( get_row_layout() === 'about' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'about' ); ?>

    <?php
    } elseif( get_row_layout() === 'services' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'services' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'career' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'career' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
