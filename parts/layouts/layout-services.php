<?php
/**
 * Description: Lionlab services
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');
$text = get_sub_field('text');
$bg = get_sub_field('bg');
$img = get_sub_field('img');
$img_text = get_sub_field('img_text');
$id = get_sub_field('anchor');

//counter
$i = 0;
?>

  <section id="<?php echo esc_attr($id); ?>" class="services bg--<?php echo esc_attr($bg); ?> padding--both">
    <div class="wrap hpad services__container">

      <h6 class="services__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
      <h2 class="services__title title"><?php echo esc_html($title); ?></h2>

      <div class="row services__row">

          <div class="services__text col-sm-6 anim fade-in">
            <?php echo $text; ?>
          </div>

          <div class="col-sm-6 center">
            <?php echo file_get_contents($img['url']); ?>
          </div>

          <div class="flex flex--wrap clearfix services__flex">

            <?php 
              if (have_rows('text_box') ) : while(have_rows('text_box') ) : the_row(); 
                $title = get_sub_field('title');
                $text = get_sub_field('text');

                $i++;
            ?>
            
            
              <div class="col-sm-4 services__item anim fade-up">
                <h6 class="meta-title">0<?php echo esc_html($i); ?>.</h6>
                <h3 class="title"><?php echo esc_html($title); ?></h3>
                <?php echo $text; ?>
              </div>
            
            
            <?php endwhile; endif; ?>

          </div>

      </div>

    </div>
  </section>