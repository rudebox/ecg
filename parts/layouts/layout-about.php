<?php
/**
 * Description: Lionlab about
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$title = get_sub_field('header');
$meta_title = get_sub_field('header_meta');
$text = get_sub_field('text');
$bg = get_sub_field('bg');
$img = get_sub_field('img');
$img_text = get_sub_field('img_text');
$id = get_sub_field('anchor');

?>

  <section id="<?php echo esc_attr($id); ?>" class="about bg--<?php echo esc_attr($bg); ?> padding--both">
    <div class="wrap hpad about__container">
      <div class="row about__row">

          <div class="col-sm-6 about__img-col">
            <div class="about__img-text anim fade-right"><?php echo $img_text; ?></div>
            <img class="about__img anim slide-up" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
          </div>

          <div class="col-sm-6 about__text anim fade-in">
            <h6 class="about__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
            <h2 class="about__title title"><?php echo esc_html($title); ?></h2>
            <?php echo $text; ?>
          </div>

      </div>
    </div>
  </section>