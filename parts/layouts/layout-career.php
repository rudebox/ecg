<?php
/**
 * Description: Lionlab career
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

  $bg = get_sub_field('bg');
  $margin = get_sub_field('margin');
  $id = get_sub_field('anchor');
  $title = get_sub_field('header');
  $meta_title = get_sub_field('header_meta');
  $text = get_sub_field('text');

  //fallback text
  $fallback = get_sub_field('fallback_text');
 ?>

 <section id="<?php echo esc_attr($id); ?>" class="career bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
   <div class="wrap hpad">
     <h6 class="career__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
     <div class="row">

        <div class="col-sm-6 career__text">
            <h2 class="career__title title"><?php echo esc_html($title); ?></h2>
            <?php echo $text; ?>
        </div>
  
        <div class="col-sm-6 career__positions">
          
          <?php 
            //count rows in careers repeater field
            if (get_row_layout() === 'career');
            $index = get_sub_field('careers');
            if (have_rows('careers') ) :
          ?>

          
            <h2 class="career__positions-title title">Ledige stillinger <span><?php echo esc_html(count($index)); ?></span></h2>  
          

          <?php endif;  ?>

          <?php if (have_rows('careers') ) :  ?>  

          <?php while (have_rows('careers') ) : the_row(); 
              $title = get_sub_field('title');
              $pdf = get_sub_field('pdf');
          ?>
        
            <a class="btn btn--round career__link" target="_blank" href="<?php echo esc_url($pdf); ?>"><?php echo $title; ?>
            </a>
            
          
          <?php endwhile; ?>

          <?php else: ?>

            <h2 class="title career__positions-title">Ledige stillinger <span>0</span></h2>

            <?php echo esc_html($fallback); ?>

          <?php endif; ?>

        </div>

     </div>
   </div>
 </section>
