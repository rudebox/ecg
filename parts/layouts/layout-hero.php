<?php
/**
 * Description: Lionlab hero
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$title = get_sub_field('hero_title');
$meta_title = get_sub_field('hero_meta_title');
$text = get_sub_field('hero_text');

?>

  <section class="hero flex">
    <div class="wrap hpad flex flex--center hero__container">
      <div class="row hero__row flex">

        <div class="col-sm-6">
          <h6 class="hero__meta-title meta-title anim fade-up"><?php echo esc_html($meta_title); ?></h6>
          <h1 class="hero__title title anim fade-up"><?php echo esc_html($title); ?></h1>

          <div class="hero__text anim fade-up">
            <?php echo $text; ?>
          </div>
        </div>

        <div class="hero__scroll">
            <div class="hero__scroll-text">
              scroll
            </div>
            <div class="hero__scroll-line">

            </div> 
        </div>
    
      </div>
    </div>
  </section>