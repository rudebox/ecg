<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$id = get_sub_field('anchor');
$meta_title = get_sub_field('slides_meta_title');

if ( have_rows('slides') ) : ?>

  <section class="slider" id="<?php echo esc_attr($id); ?>">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
          $image   = get_sub_field('slides_bg');
          $title = get_sub_field('slides_title');
          $assignment = get_sub_field('slides_assignment'); 
          $solution = get_sub_field('slides_solution');
          $return = get_sub_field('slides_return'); 
        ?>

        <div class="slider__wrap flex flex--center">

          <div class="wrap hpad slider__container">
            <div class="row flex flex--wrap">

              <div class="slider__text col-sm-6">
                <h6 class="about__meta-title meta-title"><?php echo esc_html($meta_title); ?></h6>
                <h2 class="slider__title"><?php echo $title; ?></h2>
                <h6 class="meta-title">01.</h6>
                <h3 class="title">Opgaven</h3>
                <?php echo $assignment; ?>
                <h6 class="meta-title">02.</h6>
                <h3 class="title">Løsning</h3>
                <?php echo $solution; ?>
                <h6 class="meta-title">03.</h6>
                <h3 class="title">Udbyttet</h3>
                <?php echo $return; ?>
              </div>              
              
              <div class="slider__item col-sm-6" style="background-image: url(<?php echo esc_url($image['url']); ?>);"></div>
              
            </div>
          </div>       

        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>